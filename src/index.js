

import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Cookies from "js-cookie";
import General from "./components/general";
import SideNav from "./components/sidenav";
import ProjectsList from "./components/projectsList";
import Header from "./components/header";
import AuthForm from "./components/authForm";
import Project from "./components/project";
import Developers from "./components/developers";
import DevProfile from "./components/devProfile";
import Settings from "./components/settings";
import Oauth from "./components/settings/oauth";

function isAuthneticated() {
    return Cookies.get("jwt");
}


const App = () => {
    if (isAuthneticated()) {
        return (
                <Router>
                    <SideNav/>
                    <Header/>
                    <Switch>
                        <Route path="/projects/:id" component={Project} />
                        <Route path="/projects" component={ProjectsList} />
                        <Route path="/oauth" component={Oauth} />
                        <Route path="/settings" component={Settings} />
                        <Route path="/developers/:id" component={DevProfile} />
                        <Route path="/developers" component={Developers} />
                        <Route path="/" component={General} />
                    </Switch>
                </Router>
        );
    } else {
        return <AuthForm/>;
    }
}

ReactDOM.render(<App/>, document.getElementById("app"));
