import io from "socket.io-client";

class EventsHandler {
    constructor() {
        this.client = io("http://localhost:5050");
    }

    subscribe(eventName, func) {
        this.client.on(eventName, func);
    }
}
const eventsHandler = new EventsHandler();

export default eventsHandler;
