

import { Component } from "react";
import axios from "axios";
import "../../style/header/header.css";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: null
        };
        this.getProfile().then(profile => this.setState({ profile }));
    }

    async getProfile() {
        return (await axios.get("/api/user", { withCredentials: true })).data;
    }

    render() {
        const profile = this.state.profile;
        if (profile) {
            return (
                <div className="header">
                    <img src={profile.img} alt={profile.username}/>
                    <div id="text">
                        <span>{profile.username}</span>
                        <p id="description">{profile.description}</p>
                    </div>
                </div>
            );
        } else {
            return <p className="header">Loading...</p>
        }
    }
}


export default Header;
