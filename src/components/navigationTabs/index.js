
import { Component } from "react";
import Titles from "./titles";
import Tabs from "./tabs";
import "../../style/tabs/tabs.css";

class NavigationTabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 0
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(index) {
        this.setState({
            selected: index
        });
    }

    render() {
        const components = this.props.components;
        const titles = this.props.titles;
        const selected = this.state.selected;
        return (
            <div className="tabs">
                <Titles titles={titles} selected={selected} handleClick={this.handleClick} />
                <Tabs components={components} selected={selected} />
            </div>
        );
    }
}


export default NavigationTabs;
