
import { Component } from "react";
import "../../style/tabs/titles.css";


export default function Titles(props) {
    const titles = props.titles;
    const style = { clear: "both" };
    return (
        <div className="titles">
            {
                titles.map((title, key) => {
                    const className = key === props.selected ? "title active" : "title";
                    return (
                        <div className={className} key={key} index={key} onClick={() => props.handleClick(key) }>
                            {title}
                        </div>
                    );
                })
            }
            <div style={style} ></div>
        </div>
    );
}
