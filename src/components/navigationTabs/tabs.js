
import React, { Component } from "react";
import "../../style/tabs/tabs.css";

export default function Tabs(props) {
    return (
        <div className="tab">
            { props.components[props.selected] }
        </div>
    );
}
