
import { Component } from "react";
import axios from "axios";
import CommitsCount from "./commitsCount";
import NavigationTabs from "../navigationTabs";
import "../../style/container.css";
import "../../style/devProfile/devProfile.css";

const Test = () => <div>ok</div>;

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dev: null
        };
        this.getDev().then(dev => this.setState({ dev }));
    }

    async getDev() {
        const id = this.props.match.params.id;
        return await axios.get(`/api/devs/${id}`, { withCredentials: true })
            .then(res => res.data);
    }

    render() {
        if (this.state.dev) {
            const dev = this.state.dev;
            const titles = ["general", "activities"];
            const components = [<Test/>, <Test/>];
            return (
                <div className="spacedContainer">
                    <div className="devProfile">
                        <img src={dev.avatar_url}/>
                        <div className="info">
                            <span id="name">{dev.name}</span>
                            <span id="email">{dev.public_email}</span>
                        </div>
                        <CommitsCount devId={this.state.dev.id} />
                    </div>
                    <NavigationTabs titles={titles} components={components} />
                </div>
            );
        } else {
            return <p>Loading...</p>
        }
    }
}

export default Profile;
