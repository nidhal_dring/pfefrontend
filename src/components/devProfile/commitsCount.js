
import { Component } from "react";
import axios from "axios";
import eventsHandler from "../../utils";
import "../../style/devProfile/devProfile.css";

class CommitsCount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: null
        };
        this.getCount().then(count => this.setState({ count }));
        eventsHandler.subscribe("NB_COMMITS_PER_DEV", this.onCount.bind(this));
    }

    async getCount() {
        const url = `/api/devs/${this.props.devId}/commits`;
        return axios.get(url, { withCredentials: true })
            .then(res => res.data)
            .then(data => data["NUMBER_OF_COMMITS"]);
    }

    onCount(data) {
        if (data["USER_ID"] == this.props.devId) {
            this.setState({
                count: data["NUMBER_OF_COMMITS"]
            });
        }
    }

    render() {
        if (this.state.count !== null) {
            return (
                <div className="commitsCount">
                    <p id="count">{this.state.count}</p>
                    <span>commits</span>
                </div>
            );
        } else {
            return <p> loading </p>;
        }
    }
}


export default CommitsCount;
