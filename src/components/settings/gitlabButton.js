

export default function GitlabButton() {
    const url = "https://gitlab.com/oauth/authorize?client_id=4321f850caea72fe9ccf14f6438d0f0ca8a9b4de838bf143c9ee6ad5ea9a135b&redirect_uri=http://localhost:8080/oauth&response_type=code&scope=api";
    return (
        <a href={url}>
            Use Gitlab
        </a>
    );
}
