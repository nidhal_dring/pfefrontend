

import { Component } from "react";
import axios from "axios";
import GitlabButton from "./gitlabButton";
import "../../style/container.css";

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(e) {
        e.preventDefault();
        const token = this.state.text;
        try  {
            await axios.patch("/api/user/token", { token }, { withCredentials: true });
            alert("done !");
        } catch (e) {
            alert("error !");
        }
    }

    handleChange(e) {
        this.setState({
            text: e.target.value
        });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="spacedContainer">
                <p> Enter Access Token : </p>
                <input type="text" value={this.state.text} onChange={this.handleChange}/>
                <input type="submit" value="submit"/>
                <p> or </p>
                <GitlabButton/>
            </form>
        );
    }
}


export default Settings;
