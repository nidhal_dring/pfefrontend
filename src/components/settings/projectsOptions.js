

import { Component } from "react";
import axios from "axios";
import  SuperSelect from "react-super-select";
import { Redirect } from "react-router-dom";
import "../../style/container.css";
import "../../style/react-super-select.css";


class ProjectsOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            done: false
        };
        this.options = [];
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(ops){
        this.options = ops;
    }

    async handleClick(e){
        e.preventDefault();
        const projectsIds = this.options.map(op => op.id);
        await axios.patch("/api/user/projectsIds", { projectsIds });
        this.setState({ done: true });
    }

    render() {
        if (!this.state.done) {
            return (
                <div  className="container">
                    <p>Let's Start ! </p>
                    <SuperSelect placeholder= "Select Proejcts "
                          dataSource={this.props.projects}
                          multiple={true}
                          onChange={this.handleChange}
                    />
                    <button onClick={this.handleClick} >Done</button>
                </div>
            );
        } else {
            alert("Done !");
            alert("You will now be redirected back to home page !");
            return <Redirect to="/" />
        }
    }
}

export default ProjectsOptions;
