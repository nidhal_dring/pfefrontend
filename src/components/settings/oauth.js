

import { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import ProjectsOptions  from "./projectsOptions";
import "../../style/container.css";

class Oauth extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: null
        };

        this.postCode().then(projects => this.setState({ projects }));
    }

    async postCode() {
        const query  = new URLSearchParams(window.location.search);
        const code = query.get("code");
        const res = await axios.post("/api/user/code", { code }, { withCredentials: true });
        return res.data.projects;
    }

    render() {
        if (this.state.projects) {
            return <ProjectsOptions projects={this.state.projects} />
        } else {
            return <p className="container"> working </p>
        }
    }
}


export default Oauth;
