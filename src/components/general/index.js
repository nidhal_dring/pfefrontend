
import { Component } from "react";
import {
    CommitsCard,
    IssuesCard,
    OpenMrsCard,
    ClosedMrsCard
} from "../infoCard";

class General extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <CommitsCard eventName="NB_COMMITS" backgroundColor="#25A0FF"/>
                <IssuesCard backgroundColor="#FF00D6" />
                <OpenMrsCard backgroundColor="#38FC6C" />
                <ClosedMrsCard eventName="NB_COMMITS" backgroundColor="#FF2323" />
                <div className="">

                </div>
            </div>
        );
    }
}


export default General;
