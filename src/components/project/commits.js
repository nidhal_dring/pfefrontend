
import { Component } from "react";
import axios from "axios";
import eventsHandler from "../../utils";

class Commits extends Component {
    constructor(props) {
        super(props);
        this.state = {
            commitsToday: " loading...",
            commitsMonth: " loading...",
            commitsWeek: " loading..."
        };
        this.getCommits().then(stats => this.setState(stats));
        eventsHandler.subscribe("NB_COMMITS_PER_REP_TODAY", this.OnCommitsToday.bind(this));
        eventsHandler.subscribe("COMMITS_LAST_7_DAYS", this.OnCommitsThisWeek.bind(this));
        eventsHandler.subscribe("NB_COMMITS_PER_REP_THIS_MONTH", this.OnCommitsThisMonth.bind(this));
    }

    getCommits() {
        const id = this.props.projectId;
        return axios.get(`/api/projects/${id}/commits`)
            .then(res => res.data);
    }

    OnCommitsToday(data) {
        if (data["PROJECT_ID"] == this.props.projectId) {
            this.setState({
                commitsToday: data["COUNT"]
            });
        }
    }

    OnCommitsThisWeek(data) {
        if (data["PROJECT_ID"] == this.props.projectId) {
            this.setState({
                commitsToday: data["COUNT"]
            });
        }
    }

    OnCommitsThisMonth(data) {
        if (data["PROJECT_ID"] == this.props.projectId) {
            this.setState({
                commitsMonth: data["COUNT"]
            });
        }
    }

    render() {
        return (
            <div>
                <p>commits this day: {this.state.commitsToday} </p>
                <p>commits this week: {this.state.commitsWeek} </p>
                <p>commits this month: {this.state.commitsMonth} </p>
            </div>
        );
    }
}

export default Commits;
