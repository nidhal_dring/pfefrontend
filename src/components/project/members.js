

import { Component } from "react";
import axios from "axios";
import SearchList from "../searchList";

class Members extends Component {
    constructor(props) {
        super(props);
        this.state = {
            members: null
        };
        this.getMembers().then(members => this.setState({ members }));
    }

    async getMembers() {
        const projectId = this.props.projectId;
        const res = await axios.get(`/api/projects/${projectId}/members`, { withCredentials: true });
        return res.data.members;
    }

    render() {
        if (this.state.members) {
            return <SearchList items={this.state.members} path="/developers/" />;
        } else {
            return <p>Loading...</p>;
        }
    }
}

export default Members;
