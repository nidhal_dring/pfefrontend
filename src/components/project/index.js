

import { Component } from "react";
import axios from "axios";
import ProjectInfo from "./projectInfo";
import NavigationTabs from "../navigationTabs";
import General from "./general";
import Commits from "./commits";
import Members from "./members";
import "../../style/container.css";

const Test = () => <div>ok</div>;
const Test1 = () => <div>ok1</div>;
const Test2 = () => <div>o2k</div>;


class Project extends Component {
    constructor(props) {
        super(props);
        this.state = {
            project: null
        };
        const projectId = this.props.match.params.id;
        this.getProject(projectId).then(project => this.setState({ project }));
    }

    async getProject(id) {
        return axios.get(`/api/projects/${id}`).then(res => res.data);
    }

    render() {
        if (this.state.project) {
            const titles = ["general", "members", "commits", "merge requests"];
            const components = [
                <General project={this.state.project} />,
                <Members projectId={this.state.project.id} />,
                <Commits projectId={this.state.project.id} />,
                <Test/>
            ];
            return (
                <div className="spacedContainer">
                    <ProjectInfo project={this.state.project} className="container" />
                    <NavigationTabs titles={titles}  components={components}/>
                </div>
            );
        } else {
            return <p className="container" >loading...</p>
        }
    }
}

export default Project;
