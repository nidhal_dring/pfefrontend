
import { Component } from "react";
import "../../style/container.css";
import "../../style/project/projectInfo.css";


class ProjectInfo extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const project = this.props.project;
        return (
            <div className="projectInfo">
                <img src={`https://ui-avatars.com/api/?name=${project.name}?size=256`}/>
                <span id="name"> {project.name} </span>
                <span id="visibility"> {project.visibility} </span>
            </div>
        );
    }
}

export default ProjectInfo;
