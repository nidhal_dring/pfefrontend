
import { Component } from "react";
import axios from "axios";
import SearchList from "../searchList";
import General from "./general";
import BusyDevs from "./busyDevs";
import NavigationTabs from "../navigationTabs";
import "../../style/container.css";

class Developers extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const components = [<General/>, <BusyDevs/>];
        const titles = ["general", "busy developers"];
        return (
            <div className="spacedContainer">
                <NavigationTabs components={components} titles={titles} />
            </div>
        );
    }
}

export default Developers;
