

import { Component } from "react";
import eventsHandler from "../../utils";

class BusyDevs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            devs: null
        };
        eventsHandler.subscribe("BUSY_DEVS", this.onData.bind(this));
    }

    onData(data) {
        console.log(data);
    }

    render() {
        if (this.state.devs) {
            return <div>Loading ...</div>;
        } else {
            return <div></div>;
        }
    }

}

export default BusyDevs;
