
import { Component } from "react";
import axios from "axios";
import SearchList from "../searchList";

class General extends Component {
    constructor(props) {
        super(props);
        this.state = {
            devs: null
        };
        this.getDevs().then(devs => this.setState({ devs }));
    }

    async getDevs() {
        return (await axios.get("/api/devs", { withCredentials: true }))
            .data
            .developers;
    }

    render() {
        if (this.state.devs) {
            return <SearchList items={this.state.devs} path="/developers/" />
        } else {
            return <p>Loading...</p>
        }
    }
}

export default General;
