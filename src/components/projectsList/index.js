

import { Component } from "react";
import axios from "axios";
import ProjectCard from "./projectCard";
import "../../style/container.css"

class ProjectsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: null
        }
        this.getProjects().then(projects => this.setState({ projects }));
    }

    async getProjects() {
        return (await axios.get("/api/projects")).data;
    }

    render() {
        if (this.state.projects) {
            const projects = this.state.projects;
            return (
                <div className="container">
                { projects.map((project, key) => <ProjectCard project={project} key={key} />) }
                </div>
            );
        } else {
            return <p className="container">Loading...</p>
        }
    }
}


export default ProjectsList;
