
import { Component } from "react";
import { Link } from "react-router-dom";
import "../../style/projectsList/projectCard.css";



class ProjectCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const project = this.props.project
        return (
            <div className="card">
                <div className="cardTitle">
                    {project.name}
                </div>
                <div className="cardSubTitle">
                    owned by {project.owner.name}
                </div>
                <div className="cardDescription">
                    {project.description}
                </div>
                <div className="cardTeam">
                    Team
                </div>
                <div className="cardTools">
                    <img src="public/icons/delete.png" alt="delete" />
                </div>
                <div className="cardButton">
                    <img src="public/icons/eye.png" alt="view project" />
                    <Link to={`/projects/${project.id}`}> View Project </Link>
                </div>
            </div>
        );
    }
}

export default ProjectCard;
