

import { Component } from "react";
import { Link } from "react-router-dom";
import "../../style/searchList/searchList.css";

const Item = (props) => {
    const data = props.data;
    const path = props.path;
    return (
        <li>
            <Link to={path + data.id}>
                <img src={data.img}/>
                <span>{data.name}</span>
                <span className="id">{data.id}</span>
            </Link>
        </li>
    );
}

class SearchList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: this.props.items
        };
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearch(e) {
        const text = e.target.value.toLowerCase();
        const items = this.props.items.filter(item => item.name.toLowerCase().includes(text));
        this.setState({ items });
    }

    render() {
        if (this.state.items) {
            const path = this.props.path;
            return (
                <div className="searchList">
                    <input type="text" className="search" onChange={this.handleSearch} placeholder="Search by name"/>
                    <ul>
                        {this.state.items.map((item, key) => <Item data={item} key={key} path={path} />)}
                    </ul>
                </div>
            );
        } else {
            return <p>Loading...</p>
        }
    }
}


export default SearchList;
