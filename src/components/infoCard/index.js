
import { Component } from "react";
import axios from "axios";
import eventsHandler from "../../utils";
import "../../style/infoCard/infoCard.css";

class Card extends Component {
    constructor(props, desc) {
        super(props);
        this.desc = desc;
        this.style = {
            backgroundColor: this.props.backgroundColor
        };
        this.state = {
            data: 0
        };
    }

    render() {
        return (
            <div className="infoCard" style={this.style}>
                <span>{this.state.data}</span>
                <span>{this.desc}</span>
            </div>
        );
    }
}

class CommitsCard extends Card{
    constructor(props) {
        super(props, "commits");
        this.getCommitsCount().then(data => this.setState({ data }));
        eventsHandler.subscribe(this.props.eventName, this.onData.bind(this))
    }

    async getCommitsCount() {
        return axios.get("/api/projects/commits", { withCredentials: true })
            .then(res => res.data)
            .then(data => data["NUMBER_OF_COMMITS"]);
    }

    onData(data) {
        if (this.props.projectId && data["PROJECT_ID"] !== this.props.projectId) {
            return;
        }

        this.setState({
            data: data["NUMBER_OF_COMMITS"]
        });
    }
}

class IssuesCard extends Card {
    constructor(props) {
        super(props, "issues");
        this.getIssuesCount().then(data => this.setState({ data: data.count }));
    }

    async getIssuesCount() {
        if (this.props.projectId) {
            return (await axios.get(`api/projects/${this.props.projectId}/issues`)).data;
        } else {
            return (await axios.get(`api/projects/issues`)).data;
        }
    }
}

class OpenMrsCard extends Card {
    constructor(props) {
        super(props, "open merge requests");
        this.getOpenMrsCount().then(data => this.setState({ data }));
    }

    async getOpenMrsCount() {
        return (await axios.get("/api/projects/mrs")).data.open;
    }
}

class ClosedMrsCard extends Card {
    constructor(props) {
        super(props, "closed merge requests");
        this.getClosedMrsCount().then(data => this.setState({ data }));
    }

    async getClosedMrsCount() {
        return (await axios.get("/api/projects/mrs")).data.closed;
    }
}

export {
    CommitsCard,
    IssuesCard,
    OpenMrsCard,
    ClosedMrsCard
};
