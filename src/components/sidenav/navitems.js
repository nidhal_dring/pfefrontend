
import { Component } from "react";
import NavItem from "./navitem";
import "../../style/sidenav/navitems.css";

export default class NavItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: this.createItems()
        }
    }

    createItems() {
        return [
            { text: "General", icon: "public/icons/general.png", selected: false},
            { text: "Projects", icon: "public/icons/project.png", selected: false},
            { text: "Developers", icon: "public/icons/dev.png", selected: false}
        ];
    }

    onClick(index) {
        const items = this.createItems();
        items[index].selected = true;
        this.setState({
            items
        });
    }

    render() {
        return (
            <div className="navItems">
            { this.state.items.map((item, key) => {
                return (<NavItem
                        key={key}
                        index={key}
                        icon={item.icon}
                        text={item.text}
                        selected={item.selected}
                        onClick={this.onClick.bind(this)}
                    />);
            }) }
            </div>
        );
    }
}
