
import { Link } from "react-router-dom";
import NavItems from "./navitems";
import "../../style/sidenav/sidenav.css";

class SideNav extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="sideNav">
                <p id="sidenavtitle">Servey</p>
                <Link to="/settings" id="navButton" >
                    Add Projects
                </Link>
                <input id="NavSearchBar" type="text" name="" value="Search..."/>
                <NavItems/>
            </div>
        );
    }
}


export default SideNav;
