
import { Link } from "react-router-dom";
import "../../style/sidenav/navitem.css";

export default function NavItem(props) {
    const className = props.selected ? "navItem selected" : "navItem";
    const onClick = () => props.onClick(props.index);
    return (
        <div className={className} onClick={onClick} >
            <img src={"/" + props.icon} alt="General"/>
            <Link to={"/" + props.text.toLowerCase()}> {props.text} </Link>
        </div>
    );
}
