
import { Component } from "react";
import axios from "axios";

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    async handleSubmit(e) {
        e.preventDefault();
        try {
            console.log(this.state);
            await axios.post("api/auth/login", this.state, {
                withCredentials: true
            });
            alert("login succeded !");
            window.location.reload();
        } catch (e) {
            console.log(e);
            alert("login failed ");
        }
    }

    handleChange(e) {
        const state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                Email: <input type="text" onChange={this.handleChange.bind(this)} name="email"/> <br/>
                Password: <input type="text" onChange={this.handleChange.bind(this)} name="password"/> <br/>
                <input type="submit" value="login"/>
            </form>
        );
    }
}

export default LoginForm;
