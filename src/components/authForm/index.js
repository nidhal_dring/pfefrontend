
import { Component } from "react";
import LoginForm from "./loginForm";
import RegisterForm from "./registerForm";


class AuthForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: true
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({
            login: !this.state.login
        });
    }

    render() {
        const form = this.state.login ? <LoginForm/> : <RegisterForm/>
        return (
            <div>
                { form }
                <button onClick={this.handleClick}>{ this.state.login ? "or register" : "or login" }</button>
            </div>
        );
    }
}

export default AuthForm;
