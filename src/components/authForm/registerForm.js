
import { Component } from "react";
import axios from "axios";

class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            description: "",
            username: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    async handleSubmit(e) {
        e.preventDefault();
        try {
            await axios.post("api/auth/register", this.state);
            alert("registration succeded !");
            window.location.reload();
        } catch (e) {
            console.log(e);
            alert("registration failed ");
        }
    }

    handleChange(e) {
        const state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                Email: <input type="text" name="email" onChange={this.handleChange} value={this.state.email}/> <br/>
                Description: <input type="text" name="description" onChange={this.handleChange} value={this.state.description}/> <br/>
                Username: <input type="text" name="username" onChange={this.handleChange} value={this.state.username}/> <br/>
                Password: <input type="text"  onChange={this.handleChange} name="password" value={this.state.password}/> <br/>
                <input type="submit" value="register"/>
            </form>
        );
    }
}

export default RegisterForm;
